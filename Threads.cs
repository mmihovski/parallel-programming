﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = 1000;
            double[,] array = new double[N, N];
         
            Random r = new Random();
            Stopwatch time;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                { 
                    array[i, j] = r.NextDouble(); 
                }
            }

            time = Stopwatch.StartNew();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    double sum = 0;

                    for (int k = 0; k < 200; k++)
                        sum += Math.Pow(array[i, j], k);
                }
            }
            time.Stop();
 
            Console.WriteLine("Serial {0} s", time.Elapsed.TotalSeconds);

            time.Reset();

            Thread thread1 = new Thread ( () =>
            {
                for (int i = 0; i < array.GetLength(0) / 2; i++)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        double sum = 0;

                        for (int k = 0; k < 200; k++)
                        {
                            sum += Math.Pow(array[i, j], k);
                        }
                    }
                }
            }  );


            Thread thread2 = new Thread ( () =>
            {
                for (int i = array.GetLength(0) / 2; i < array.GetLength(0); i++)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        double sum = 0;

                        for (int k = 0; k < 200; k++)
                        {
                            sum += Math.Pow(array[i, j], k);
                        }
                    }
                }
            }  );

            time = Stopwatch.StartNew();

            thread1.Start();
            thread2.Start();

            while (thread1.IsAlive && thread2.IsAlive) { }
            
            time.Stop();

            Console.WriteLine("Threads {0} s", time.Elapsed.TotalSeconds);
            Console.Read();
        }
    }
}
