﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static double[,] array = new double[1000, 1000];
        static void Main(string[] args)
        {

            Random random = new Random();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    array[i, j] = random.NextDouble();
                }
            }

            Thread t1 = new Thread( () => Calculate() );
            t1.Priority = ThreadPriority.Lowest;
            t1.Name = "T1";


            Thread t2 = new Thread( () => Calculate() );
            t2.Priority = ThreadPriority.Highest;
            t2.Name = "T2";

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Console.Read();
        }

        public static void Calculate()
        {
            double sum = 0;

            Stopwatch sw = Stopwatch.StartNew();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (i % 10 == 0)
                {
                    Console.Write(Thread.CurrentThread.Name + "\t");
                }
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    sum += array[i, j];
                }
            }

            sw.Stop();
            Console.WriteLine("\n\n  {0} ,  {1:f8} ms \n\n", Thread.CurrentThread.Name, sw.Elapsed.TotalMilliseconds);
        }
    }
}
