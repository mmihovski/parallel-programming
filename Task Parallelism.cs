﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskParallelism
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[10000];


            for (int i = 0; i < array.Length; i++)
            {
                array[i] = i;
            }
            TaskParallelism(array);

            Console.ReadLine();
        }

        static void MultiplyArray(int [] array, int startIndex, int endIndex)
        {
            Console.WriteLine("MultiplyArray. Task={0}, Thread={1}, start={2}, end={3}", 
                Task.CurrentId, Thread.CurrentThread.ManagedThreadId, startIndex, endIndex);

            for (int i = startIndex; i < endIndex; i++)
            {
                array[i] *= 2;
            }
        }
        static void TaskParallelism(int [] array)
        {
            int processorCount = Environment.ProcessorCount;
          
            Task[] tasks = new Task[processorCount];
           
            for (int i = 0; i < processorCount; i++)
            {
                int j = i;
                tasks[i] = Task.Factory.StartNew
                (  () => MultiplyArray (array, (array.Length / processorCount) * j, (array.Length / processorCount) * (j + 1)  ) );
            }
            Task.WaitAll(tasks);
        }
    }
}