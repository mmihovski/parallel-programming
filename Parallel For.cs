﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static double[] array = new double[100000];
        static Stopwatch time;
        static double sum;

        private static void Calculate (double a)
        {
            sum = 0;

            for (int k = 0; k < 250; k++)
            { 
                sum += (Math.Pow(a, k) + Math.Sqrt(array[k])); 
            }
        }

        static void Main(string[] args)
        {
            Random generator = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = generator.Next(100, 1000);
            }

            time = Stopwatch.StartNew();

            Parallel.For(0, array.Length, i =>
            {
                Calculate(array[i]);
            } );

            time.Stop();

            Console.WriteLine("parallel for: " + time.Elapsed.TotalSeconds + " s");

            time.Reset();


            time = Stopwatch.StartNew();

            for (int i = 0; i < array.Length; i++)
            {
                Calculate(array[i]);
            }
            
            time.Stop();

            Console.WriteLine("for: " + time.Elapsed.TotalSeconds + " s" );

            time.Reset();
           
            Console.Read();
        }
    }
}
